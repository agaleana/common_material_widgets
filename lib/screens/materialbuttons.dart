import 'package:flutter/material.dart';

class MaterialButtons extends StatelessWidget {
  final isSelected = <bool>[true, false, false];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Buttons'),
      ),
      body: Center(
        child: Card(
          elevation: 0,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              // Flat button: less-pronounced actions. Text Buttons help maintain an emphasis on card content
              FlatButton.icon(
                onPressed: () {},
                icon: Icon(Icons.add, size: 18),
                label: Text('Flat Button'),
              ),
              // Outlined button: medium-emphasis buttons. For important actions, but aren’t the primary action in an app.
              OutlineButton.icon(
                onPressed: () {},
                icon: Icon(Icons.add, size: 18),
                label: Text('Outlined Button'),
              ),
              // Raised button: high-emphasis. They have elevation and fill. For actions important in the app.
              RaisedButton.icon(
                onPressed: () {},
                icon: Icon(Icons.add, size: 18),
                label: Text('Raised Button'),
              ),
              // Icon button:toggle buttons that allow selection or deselection. Example: mark an item as a favorite
              ToggleButtons(
                isSelected: isSelected,
                onPressed: (index) {
                  // Respond to button selection
                  setState(() {
                    isSelected[index] = !isSelected[index];
                  });
                },
                children: <Widget>[
                  Icon(Icons.favorite),
                  Icon(Icons.visibility),
                  Icon(Icons.notifications),
                ],
              ),
              // Toogle button: emphasis in groups of related toggle buttons. A group should share a common container.
              ToggleButtons(
                isSelected: isSelected,
                onPressed: (index) {
                  // Respond to button selection
                  setState(() {
                    isSelected[index] = !isSelected[index];
                  });
                },
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16.0),
                    child: Text('BUTTON 1'),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16.0),
                    child: Text('BUTTON 2'),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16.0),
                    child: Text('BUTTON 3'),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  void setState(Null Function() param0) {}
}
