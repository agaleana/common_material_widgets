import 'package:flutter/material.dart';

class MaterialDataTables extends StatelessWidget {
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: (AppBar(
        title: Text('Data Tables'),
      )),
      body: Center(
        child: ListView(
          padding: EdgeInsets.all(16),
          children: [
            PaginatedDataTable(
              rowsPerPage: 5,
              header: Text('Employees'),
              columns: [
                DataColumn(label: Text('First Name (string)')),
                DataColumn(label: Text('Last Name (string)')),
                DataColumn(label: Text('Department (string)')),
                DataColumn(label: Text('Tenure (double)')),
              ],
              source: _DataSource(context),
            ),
          ],
        ),
      ),
    );
  }
}

class _Row {
  _Row(
    this.firstName,
    this.lastName,
    this.department,
    this.tenure,
  );

  final String firstName;
  final String lastName;
  final String department;
  final double tenure;

  bool selected = false;
}

class _DataSource extends DataTableSource {
  _DataSource(this.context) {
    _rows = <_Row>[
      _Row('Mickey', 'Mouse', 'Magic Kingdom', 92),
      _Row('SpongeBob', 'SquarePants', 'Chum Bucket', 21),
      _Row('Homer', 'Simpson', 'Power Plant Safety', 33),
      _Row('Bugs', 'Bunny', 'Animation', 82),
      _Row('Wilma', 'Flintstone', 'Social Media', 61),
    ];
  }

  final BuildContext context;
  List<_Row> _rows;

  int _selectedCount = 0;

  @override
  DataRow getRow(int index) {
    assert(index >= 0);
    if (index >= _rows.length) return null;
    final row = _rows[index];
    return DataRow.byIndex(
      index: index,
      selected: row.selected,
      onSelectChanged: (value) {
        if (row.selected != value) {
          _selectedCount += value ? 1 : -1;
          assert(_selectedCount >= 0);
          row.selected = value;
          notifyListeners();
        }
      },
      cells: [
        DataCell(Text(row.firstName)),
        DataCell(Text(row.lastName)),
        DataCell(Text(row.department)),
        DataCell(Text(row.tenure.toString())),
      ],
    );
  }

  @override
  int get rowCount => _rows.length;

  @override
  bool get isRowCountApproximate => false;

  @override
  int get selectedRowCount => _selectedCount;
}
