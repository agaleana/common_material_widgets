import 'package:flutter/material.dart';

class MaterialTextFields extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Text Fields'),
      ),
      body: Card(
        margin: EdgeInsets.all(10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            TextFormField(
              initialValue: 'Default text',
              decoration: InputDecoration(
                icon: Icon(Icons.favorite),
                labelText: 'Label text',
                helperText: 'Helper text',
                prefixIcon: Icon(
                  Icons.alarm,
                ),
                suffixIcon: Icon(
                  Icons.check_circle,
                ),
                enabledBorder: UnderlineInputBorder(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
