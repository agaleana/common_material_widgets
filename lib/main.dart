import 'package:common_material_widgets/screens/materialbuttons.dart';
import 'package:common_material_widgets/screens/materialdatatables.dart';
import 'package:common_material_widgets/screens/materialtextfields.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(FlutterWorkshop());
}

class FlutterWorkshop extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text('Material Common Widgets'),
        ),
        body: Builder(
          builder: (context) => ListView(children: <Widget>[
            ListTile(
                title: Text('Buttons'),
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => MaterialButtons()));
                }),
            ListTile(
                title: Text('Text Fields'),
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => MaterialTextFields()));
                }),
            ListTile(
                title: Text('Data Tables'),
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => MaterialDataTables()));
                }),
          ]),
        ),
      ),
    );
  }
}
