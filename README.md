# Common Material Widgets

This is a simple Flutter app with common material widgets and its most basic implementation


This project is a starting point for a Flutter application.

Please visiti Material's official documentation at 

- [Material](https://https://material.io/)
- [Material - Develop](https://material.io/develop)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
